from config import cfg, cfg_from_file
import numpy as np
import pickle
from PIL import Image
from collections import defaultdict
import random
from nltk.tokenize import RegexpTokenizer
import os

class TextDataset():
    def __init__(self, data_dir, base_size=64, split="train",
                 transform=None, target_transform=None):
        
        self.target_transform = target_transform
        self.embeddings_num = cfg.TEXT.CAPTIONS_PER_IMAGE

        self.data = []
        self.data_dir = data_dir

        split_dir = os.path.join(data_dir, split)
        self.split_dir = split_dir

        self.train_filenames, self.train_captions, self.test_filenames, \
        self.test_captions, self.ixtoword, self.wordtoix, self.n_words = \
        self.load_text_data(data_dir, split)
        
        print(len(self.train_captions), len(self.test_captions))

        self.train_class_id = self.load_class_ids(
                                              os.path.join(data_dir,"train"))
        self.test_class_id = self.load_class_ids(
                                              os.path.join(data_dir,"test"))
        self.train_number_example = len(self.train_filenames)
        self.test_number_example = len(self.test_filenames)
        self.start = 0
        self.train_data_order = np.arange(self.train_number_example)
        self.test_data_order = np.arange(self.test_number_example)
        np.random.shuffle(self.train_data_order)

        self.embedding_index = 0
        
    def load_captions(self, data_dir, filenames, class_ids):
        all_captions = []
        for i in range(len(filenames)):
            cap_path = '%s/text_c10/class_%05d/%s.txt' % (data_dir, 
                                                class_ids[i], filenames[i][4:])
            with open(cap_path, "r") as f:
                captions = f.read().split('\n')
                cnt = 0
                for cap in captions:
                    if len(cap) == 0:
                        continue
                    cap = cap.replace("\ufffd\ufffd", " ")
                    # picks out sequences of alphanumeric characters as tokens
                    # and drops everything else
                    tokenizer = RegexpTokenizer(r'\w+')
                    tokens = tokenizer.tokenize(cap.lower())
                    if len(tokens) == 0:
                        all_captions.append(all_captions[-1])
                        cnt += 1
                        continue

                    tokens_new = []
                    for t in tokens:
                        t = t.encode('ascii', 'ignore').decode('ascii')
                        if len(t) > 0:
                            tokens_new.append(t)

                    all_captions.append(tokens_new)
                    cnt += 1
                    if cnt == self.embeddings_num:
                        break
                assert (cnt == self.embeddings_num), "Nr. of caps not correct!"
        return all_captions

    def build_dictionary(self, train_captions, test_captions):
        word_counts = defaultdict(float)
        captions = train_captions + test_captions
        for sent in captions:
            for word in sent:
                word_counts[word] += 1

        vocab = sorted([w for w in word_counts if word_counts[w] >= 0])

        ixtoword = {}
        ixtoword[0] = '<end>'
        wordtoix = {}
        wordtoix['<end>'] = 0
        ix = 1
        for w in vocab:
            wordtoix[w] = ix
            ixtoword[ix] = w
            ix += 1

        train_captions_new = []
        for t in train_captions:
            rev = []
            for w in t:
                if w in wordtoix:
                    rev.append(wordtoix[w])
            # rev.append(0)  # do not need '<end>' token
            train_captions_new.append(rev)

        test_captions_new = []
        for t in test_captions:
            rev = []
            for w in t:
                if w in wordtoix:
                    rev.append(wordtoix[w])
            # rev.append(0)  # do not need '<end>' token
            test_captions_new.append(rev)

        return [train_captions_new, test_captions_new,
                ixtoword, wordtoix, len(ixtoword)]

    def load_text_data(self, data_dir, split):
        filepath = os.path.join(data_dir, 'captions.pickle')
        train_names = self.load_filenames(data_dir, 'train')
        test_names = self.load_filenames(data_dir, 'test')
        
        train_class_ids = self.load_class_ids(os.path.join(data_dir,"train"))
        test_class_ids = self.load_class_ids(os.path.join(data_dir, "test"))

        train_captions = self.load_captions(data_dir, train_names,
                                                      train_class_ids)
        test_captions = self.load_captions(data_dir, test_names, 
                                                      test_class_ids)
        train_captions, test_captions, ixtoword, wordtoix, n_words = \
            self.build_dictionary(train_captions, test_captions)
        with open(filepath, 'wb') as f:
            pickle.dump([train_captions, test_captions,
                         ixtoword, wordtoix], f, protocol=2)
            print('Writing captions to: ', filepath)
        return (train_names, train_captions, test_names, test_captions,
                ixtoword, wordtoix, n_words)

    def load_class_ids(self, data_dir):
        if os.path.isfile(data_dir + '/class_info.pickle'):
            with open(data_dir + '/class_info.pickle', 'rb') as f:
                class_ids = pickle.load(f)
        else:
            print("ERROR: Wrong path to class ids!")
            print("Path: %s" % data_dir )
        return class_ids

    def load_filenames(self, data_dir, split):
        filepath = '%s/%s/filenames.pickle' % (data_dir, split)
        if os.path.isfile(filepath):
            with open(filepath, 'rb') as f:
                filenames = pickle.load(f)
            print('Load filenames from: %s (%d)' % (filepath, len(filenames)))
        else:
            filenames = []
        return filenames

    def get_caption(self, sent_ix, split):
        if split == "test":
            captions = self.test_captions
        else: 
            captions = self.train_captions
        # a list of indices for a sentence
        sent_caption = np.asarray(captions[sent_ix]).astype('int64')
        if (sent_caption == 0).sum() > 0:
            print('ERROR: do not need END (0) token', sent_caption)
        num_words = len(sent_caption)
        # pad with 0s (i.e., '<end>')
        x = np.zeros((cfg.TEXT.WORDS_NUM, 1), dtype='int64')
        x_len = num_words
        if num_words <= cfg.TEXT.WORDS_NUM:
            x[:num_words, 0] = sent_caption
        else:
            ix = list(np.arange(num_words))  # 1, 2, 3,..., maxNum
            np.random.shuffle(ix)
            ix = ix[:cfg.TEXT.WORDS_NUM]
            ix = np.sort(ix)
            x[:, 0] = sent_caption[ix]
            x_len = cfg.TEXT.WORDS_NUM
        return x, x_len


    def get_img(self, img_path, imsize):
        img = Image.open(img_path).convert('RGB')
        # scale image to [-1, 1]
        
        if random.random() > 0.5:
            img = np.array(img.resize([400, 400]))
            startx, starty = random.randint(0, 100), random.randint(0,100)
            img = img[startx:startx+imsize[0], starty:starty+imsize[1]]
        else: 
            img = np.array(img.resize(imsize))
        if random.random() > 0.5: 
            img = np.fliplr(img)
        img = img * (2. / 255.) - 1        
        return img

    
    def get_next_batch(self, batch_size, split="train",\
                       get_fake_img=False, imsize=[299, 299]):
        if split == "train":
            if self.start >= self.train_number_example:
                self.train_data_order = np.arange(self.train_number_example)
                np.random.shuffle(self.train_data_order)
                self.start = 0
            idcs = self.train_data_order[self.start:self.start+batch_size]
        else:
            if self.start >= self.test_number_example:
                self.test_data_order = np.arange(self.test_number_example)
                np.random.shuffle(self.test_data_order)
                self.start = 0
            idcs = self.test_data_order[self.start:self.start+batch_size]
        self.start = self.start + batch_size
        batch = list()
        for idx in idcs:
            batch.append(self.__get_item__(idx, split, get_fake_img, 
                                           imsize[0], imsize[1]))
        return batch


    def __get_item__(self, index, split="train", get_fake_img=True, \
                     w=299, h=299, cap_idx=None):
        if split == "test":
            key = self.test_filenames[index]
            cls_id = self.test_class_id[index]
        else:
            key = self.train_filenames[index]
            cls_id = self.train_class_id[index]
        if get_fake_img:
            collision_flag = True
            while collision_flag:
                if split == "train":
                    fake_idx = random.randint(0, len(self.train_class_id) - 1)
                    if self.train_class_id[fake_idx] != self.train_class_id[index]:
                        collision_flag = False
                        fake_key = self.train_filenames[fake_idx]
                        fake_cls_id = self.train_class_id[fake_idx]
                else: 
                    fake_idx = random.randint(0, len(self.test_class_id) - 1)
                    if self.test_class_id[fake_idx] != self.test_class_id[index]:
                        collision_flag = False
                        fake_key = self.test_filenames[fake_idx]
                        fake_cls_id = self.test_class_id[fake_idx]
            fake_img_name = '%s/%s.jpg' % (self.data_dir, fake_key)
            fake_img = self.get_img(fake_img_name, [w,h])
        else:
            fake_img = None 

        img_name = '%s/%s.jpg' % (self.data_dir, key)
        img = self.get_img(img_name, [w,h])
        # random select a sentence
        if cap_idx is not None:
            sent_ix = cap_idx
        else:
            sent_ix = random.randint(0, self.embeddings_num - 1)
        new_sent_ix = index * self.embeddings_num + sent_ix
        caps, cap_len = self.get_caption(new_sent_ix, split)
        return img, fake_img, caps, cap_len, cls_id, key

    def __get_next_batch_ordered__(self, split="train"):
        """
        Returns batch of the next 50 entries in order.
        An entry is image + caption. Because there are 10 caps
        for each image, a batch should contain 5 images with all
        combinations of their captions.
        Used for extraction of word/sentence embeddings
        """
        if split == "train":
            if self.embedding_index <= self.train_number_example - 5:
                idcs = np.arange(self.embedding_index, self.embedding_index+5)
                self.embedding_index += 5
            else: 
                if self.embedding_index >= self.train_number_example:
                    self.embedding_index = 0
                    return None
                else:
                    idcs = np.arange(self.embedding_index,
                                     self.train_number_example)
                    idcs = list(idcs)
                    padding_idcs = list(np.arange(0, 5 - len(idcs)))
                    idcs = idcs + padding_idcs
                    self.embedding_index += 5   

        if split == "test":
            if self.embedding_index <= self.test_number_example - 5:
                idcs = np.arange(self.embedding_index, self.embedding_index+5)
                self.embedding_index += 5
            else: 
                if self.embedding_index >= self.test_number_example:
                    self.embedding_index = 0
                    return None
                else:
                    idcs = np.arange(self.embedding_index,
                                     self.test_number_example)
                    idcs = list(idcs)
                    padding_idcs = list(np.arange(0, 5 - len(idcs)))
                    idcs = idcs + padding_idcs
                    self.embedding_index += 5   
        batch = list()
        for idx in idcs:
            for sent_ix in range(10):
                batch.append(self.__get_item__(idx, split, False, 
                                               299, 299, sent_ix))
        assert len(batch) == 50, "Batch of size %d instead 50" % len(batch)
        return batch


    def __len__(self):
        return len(self.train_filenames, split="train")
