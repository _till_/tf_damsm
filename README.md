# DAMSM Implementation in Tensorflow
This repo contains an implementation of the Deep Attentional Multimodal Similarity Model introduced in [the AttnGAN paper](http://openaccess.thecvf.com/content_cvpr_2018/html/Xu_AttnGAN_Fine-Grained_Text_CVPR_2018_paper.html).

Pretrained embeddings for the Oxford flowers-102 dataset can be found in the `embeddings` directory.
Reference code in Pytorch is provided by the authors of mentioned paper [here](https://github.com/taoxugit/AttnGAN).

## Functionalities
The `pretrain_DAMSM.py` script provides functionality for training and inference, you can set traiing/inference mode with the command line arguments.
`python pretrain_DAMSM.py --training`
After training the DAMSM, the embeddings of captions and words can be extracted using this command:
`python pretrain_DAMSM.py --extract_embeddings`

The equation numbers used in the code comments reference the equations in [the AttnGAN paper](http://openaccess.thecvf.com/content_cvpr_2018/html/Xu_AttnGAN_Fine-Grained_Text_CVPR_2018_paper.html).


