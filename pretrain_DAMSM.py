import numpy as np
import os.path as osp
import os
import argparse
import pickle
from config import cfg
import tensorflow as tf
import tensorflow_hub as hub
from tensorflow.layers import average_pooling2d, conv2d, dense
import TextDataset
from utils import masked_softmax, unbatch_embeddings
from tensorflow.keras.backend import repeat_elements

class DAMSM(object):
    
    def __init__(self, bs):
        self.bs = bs
        self.global_step=tf.Variable(0, name="global_step", trainable=False)
        self.build_model()
        self.define_losses()
        self.define_summaries()

    def build_model(self):
        self.is_training = tf.placeholder(tf.bool)
        self.init_cnn_encoder()
        # img and caption
        self.imgs = tf.placeholder(tf.float32, shape=(None, 299, 299, 3))
        
        # for descriptions padded to len 18
        self.caps = tf.placeholder(tf.int32, shape=(None, 18))
        
        # at pos.  [i, j] whether imgs i,j are from same class
        self.class_mask = tf.placeholder(tf.float32)
        self.cap_lens = tf.placeholder(tf.int32, shape=(self.bs))
        self.labels = tf.one_hot(np.array(range(self.bs)), 
                                    depth=self.bs,
                                    dtype=tf.int32)

        self.class_ids = tf.placeholder(tf.int32, shape=(None, 1))
        self.eps=1e-8
        self.build_lstm_encoder()
        self.hidden_states = self.lstm_encoder(self.caps)

        # e: feat. matrix of all words, bs x D (wordvec-dim) x T (nr. of words)
        self.wordf = tf.transpose(self.hidden_states, [0, 2, 1])
        
        # e_bar: sentence feature vector, bs x D
        self.sentf = tf.concat([self.hidden_states[:, -1, :128], 
                                self.hidden_states[:, 0, 128:]], axis=1)     
        self.img_localf, self.img_globalf = self.cnn_encoder(self.imgs)
    
    
    def define_losses(self):
        self.define_sent_losses()
        self.define_word_losses()
        self.total_loss = self.word_loss0 + self.word_loss1 \
                          + self.sent_loss0 + self.sent_loss1
        self.optimizer = tf.train.AdamOptimizer(learning_rate=0.0002, 
                                                beta1=0.5, beta2=0.999)
        self.optim = self.optimizer.minimize(self.total_loss,
                                             global_step=self.global_step)
    
    def define_sent_losses(self):
        # Assuming bs of 16
        #self.masks = tf.placeholder(tf.uint8, shape=(16, 16))
        self.sentf_norm = tf.norm(self.sentf, ord=2, axis=1, keepdims=True)
        self.globalf_norm = tf.norm(self.img_globalf, ord=2,
                                    axis=1, keepdims=True)
        
        self.scores0 = tf.matmul(self.img_globalf,
                                 tf.transpose(self.sentf, perm=[1, 0]))
        self.norm0 = tf.matmul(self.globalf_norm,
                               tf.transpose(self.sentf_norm, perm=[1, 0]))
        
        eps_ = tf.fill(tf.shape(self.norm0), self.eps)                    
        self.scores0 = tf.divide(self.scores0,
                                 tf.where(self.norm0 < self.eps,
                                          eps_, self.norm0)) \
                       * cfg.TRAIN.SMOOTH.GAMMA3

        # --> batch_size x batch_size
        self.scores0 = tf.squeeze(self.scores0)
        
        # masking - give 0 prob to descriptions of imgs of same class
        self.inf_class_mask = tf.int32.max * self.class_mask
        self.scores0 = self.scores0 - self.inf_class_mask
       
        self.scores1 = tf.transpose(self.scores0, perm=[1, 0])

        sent_loss0 = tf.nn.softmax_cross_entropy_with_logits_v2(
                                       labels=self.labels, logits=self.scores0)
        self.sent_loss0 = tf.reduce_mean(sent_loss0)
        sent_loss1 = tf.nn.softmax_cross_entropy_with_logits_v2(
                                       labels=self.labels, logits=self.scores1)
        self.sent_loss1 = tf.reduce_mean(sent_loss1)
    
    def define_word_losses(self):
        # equation 7:
        # e: bs x D (dim of word vec) x T (max. len of captions)
        # v: bs x D x 289
        self.sim = tf.matmul(tf.transpose(self.wordf, perm=[0, 2, 1]), 
                             self.img_localf)
        
        # equation 8:
        # make sure padding "words" get 0 similarity score
        self.sim_mask = tf.sequence_mask(self.cap_lens,
                                         maxlen=18)
        self.sim_norm = masked_softmax(self.sim, self.sim_mask)

        # equation 9:
        self.alphas = tf.nn.softmax(logits=cfg.TRAIN.SMOOTH.GAMMA1 
                                          * self.sim_norm)
        self.cs = self.alphas @ tf.transpose(self.img_localf, perm=[0, 2, 1])

        # equation 10:
        # 10.1: the relevance between the ith word and the image 
        # print(tf.nn.l2_normalize(self.cs, 2).shape)
        # print(tf.nn.l2_normalize(self.wordf, 1).shape)
        
        c_norm = tf.nn.l2_normalize(tf.transpose(self.cs, [0,2,1]), 1)
        e_norm = tf.nn.l2_normalize(self.wordf, 1)
        self.cosine_distances = tf.reduce_sum(tf.multiply(c_norm, e_norm),
                                                          axis=1)
        
        """
        The region context vectors of all images in the batch need to be
        multiplied with the word vectors of all the descriptions in the batch
        in order to compare the scores.
        The first bs entries in c_norm_dup contain the context vecor matrix of
        the first image and so on.
        e_norm_tiled contains bs many copies of the word vector matrices e,
        stacked.
        """
        c_norm_dup = repeat_elements(c_norm, self.bs, axis=0)
        self.e_norm_tiled = tf.tile(e_norm, [self.bs, 1, 1])
        
        """
        Should contain results in following order:
        first_contexts, first_captions,
        first_contexts, second_captions
        ....
        first_contexts, batch_size'th_captions
        second_contexts, first_captions,
        ....
        """
        self.all_cos_sims = tf.reduce_sum(tf.multiply(c_norm_dup,
                                                      self.e_norm_tiled),
                                                      axis=1)
        
        # equation 10
        itm_scores = tf.reduce_sum(tf.exp(cfg.TRAIN.SMOOTH.GAMMA2 
                                          * self.all_cos_sims), axis=1)
        self.itm_scores = tf.log(tf.pow(itm_scores,
                                        1. / cfg.TRAIN.SMOOTH.GAMMA2))
        
        """
        equations 11, 12, 13 combined:
        Image text matching scores between contexts and captions
        e.g. itm_scores[0][1] contains score between first image 
        and second captions
        first dim: context
        second dim: captions
        """
        self.itm_scores = tf.reshape(self.itm_scores, [self.bs, self.bs])
        
        # mask out scores for different samples of same class
        self.itm_scores = self.itm_scores - self.inf_class_mask
        word_loss0 = tf.nn.softmax_cross_entropy_with_logits_v2(
                                 labels=self.labels,
                                 logits=self.itm_scores,
                                 name="word_loss0")
        self.word_loss0 = tf.reduce_mean(word_loss0)    
    
        word_loss1 = tf.nn.softmax_cross_entropy_with_logits_v2(
                                 labels=self.labels,
                                 logits=tf.transpose(self.itm_scores, 
                                                     perm=[1,0]))
        self.word_loss1 = tf.reduce_mean(word_loss1)    
    
    def define_summaries(self):
        tf.summary.scalar('overall_loss', self.total_loss) 
        tf.summary.scalar('word_loss_0', self.word_loss0) 
        tf.summary.scalar('word_loss_1', self.word_loss1) 
        tf.summary.scalar('sent_loss_0', self.sent_loss0) 
        tf.summary.scalar('sent_loss_1', self.sent_loss1) 
        self.merged = tf.summary.merge_all() 
        
    def init_cnn_encoder(self):
        os.environ['TFHUB_CACHE_DIR'] = "/home/users/tilic/project/" + \
                                      "text-to-image/workdir/tf_damsm/tf_cache"
        
        self.module = hub.Module("https://tfhub.dev/google/imagenet/" + 
                                 "inception_v3/classification/3",
                                 trainable=False)
        
    def initialize_uninitialized(self, sess):
        global_vars = tf.global_variables()
        is_not_initialized = sess.run([tf.is_variable_initialized(var) 
                                       for var in global_vars])
        not_initialized_vars = [v for (v, f) in
                                zip(global_vars, is_not_initialized) if not f]
        print('Initializing %d variables:\n' % len(not_initialized_vars))
        if len(not_initialized_vars):
            sess.run(tf.variables_initializer(not_initialized_vars))
    
    def cnn_encoder(self, images):
        with tf.variable_scope("cnn_enc", reuse=False):
            # stores all layers in key-value pairs
            module_features = self.module(dict(images=images),
                                          signature='image_classification',
                                          as_dict=True)
            # batch size x 17 x 17 x 768
            local_features = module_features['InceptionV3/Mixed_6e']

            # batch size x 17 x 17 x 256
            # 256 is the filter dimension in attngan code
            local_features = conv2d(inputs=local_features, 
                                    filters=256, kernel_size=1)
            
            # bs x 289 x 256
            local_features = tf.reshape(local_features, [-1, 289, 256])
            local_features = tf.transpose(local_features, [0, 2, 1])
            
            # bs x 289 x 256
            global_features = module_features['InceptionV3/Mixed_7c']
            
            # bs x 1 x 1 x 2048
            global_features = average_pooling2d(global_features, 
                                                pool_size=8, strides=1)
            global_features = dense(global_features, 256)
            
            # batch size x 256
            global_features = tf.squeeze(global_features, axis=[1,2])
            
            return local_features, global_features
        
    
    def build_lstm_encoder(
            self, ntoken=5428, ninput=300, drop_prob=0.5, nhidden=128):
        self.embedding = tf.keras.layers.Embedding(input_dim=ntoken, 
                                    output_dim=ninput,
                                    embeddings_initializer= \
                                    tf.keras.initializers.RandomUniform(
                                                      minval=-0.1, maxval=0.1))
        
        #TODO: look at what self.drop_prob should be
        self.drop_prob = 0.5
        
        # might be better to change to tf.compat.v1.keras.layers.CuDNNLSTM
        self.lstm = tf.keras.layers.LSTM(return_sequences=True,  
                                         units=nhidden, time_major=False,
                                         dropout=self.drop_prob,
                                         recurrent_dropout=self.drop_prob,
                                         )
        
        self.rnn = tf.keras.layers.Bidirectional(layer=self.lstm,
                                                 merge_mode="concat")
        
    
    def lstm_encoder(self, caps):
        with tf.variable_scope("lstm_enc", reuse=False):
            emb = self.embedding(caps)
            emb = self.rnn(emb, training=self.is_training)
            return emb
            
    def prepare_data_batch(self, batch):
        imarrays, caps, cap_lens, class_ids = [], [], [], []
        for t in batch:
            imarrays.append(t[0])
            caps.append(np.squeeze(t[2]))
            cap_lens.append(np.squeeze(t[3]))
            class_ids.append(t[4])
        imbatch = np.stack(imarrays, axis=0)
        capsbatch = np.stack(caps, axis=0)
        cap_lens_batch = np.stack(cap_lens, axis=0)
        class_mask = np.zeros([len(class_ids), len(class_ids)], dtype=np.int8)
        for j in range(len(class_ids)):
            for k in range(len(class_ids)):
                if j != k and class_ids[j] == class_ids[k]:
                    class_mask[j, k] = 1
        return imbatch, capsbatch, cap_lens_batch, class_mask

    def train(self, dataset, bs):
        with tf.Session() as sess:
            saver = tf.train.Saver(max_to_keep=5)
            logdir = "/home/users/tilic/cache/tensorboard-logdir/" + \
                     "text-to-image/tfdamsm2"
            train_writer = tf.summary.FileWriter(logdir, sess.graph)
            checkpoint_dir="./checkpoints"
            ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
            if ckpt and ckpt.model_checkpoint_path:
                sess.run(tf.global_variables_initializer())
                ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
                saver.restore(sess, os.path.join(checkpoint_dir, ckpt_name))
                print(" [*] Success to read {}".format(ckpt_name))
            else:
                self.initialize_uninitialized(sess)
                print(" [*] Failed to find checkpoints")
            
            train_losses, val_losses = [], []
            epoch = 0
            for i in range(1000000):
                # list of tuples: imgs, caps, cap_len, cls_id, key
                cur_batch = dataset.get_next_batch(batch_size=bs)
                imbatch, capsbatch, cap_lens_batch, class_mask = \
                                             self.prepare_data_batch(cur_batch)
                feed_dict={
                           self.is_training: True,
                           self.imgs: imbatch,
                           self.caps: capsbatch,
                           self.cap_lens: cap_lens_batch,
                           self.class_mask: class_mask
                }        
                if len(imbatch) == self.bs: 
                    loss, _, summary, step = sess.run(fetches=[self.total_loss,
                                                      self.optim, self.merged,
                                                      self.global_step], 
                                                feed_dict=feed_dict)
                    train_writer.add_summary(summary, step)
                    train_writer.flush()
                    train_losses.append(loss) 
                if (epoch + 1) * dataset.train_number_example < i * bs:
                    epoch += 1
                    print("Start epoch %d" % epoch)
                if np.mod(step, 100) == 0:
                    print("Train loss after %d steps: "% step, np.mean(loss))
                    val_batch = dataset.get_next_batch(bs, "test")
                    imbatch, capsbatch, cap_lens_batch, class_mask = \
                                             self.prepare_data_batch(val_batch)

                    feed_dict={
                           self.is_training: False,
                           self.imgs: imbatch,
                           self.caps: capsbatch,
                           self.cap_lens: cap_lens_batch,
                           self.class_mask: class_mask
                    }        
                    if len(val_batch) == self.bs: 
                        val_loss, = sess.run(fetches=[self.total_loss], 
                                                feed_dict=feed_dict)
                        print("Validation loss after %d steps : " % step,
                              np.mean(val_loss))
                        val_losses.append(val_loss)
                    else:
                        print("Wrong batch size for validation data!")
                if np.mod(step, 1000) == 0:
                    saver.save(sess, checkpoint_dir + '/DAMSM_model', 
                               global_step=step)
                    with open("train_losses.pkl", "wb") as f:
                        pickle.dump(train_losses, f)
                    with open("val_losses.pkl", "wb") as f:
                        pickle.dump(val_losses, f)
            return res
    
    def extract_embeddings(self):
        dataset = TextDataset.TextDataset(
                            data_dir="/home/users/tilic/project/" 
                                     + "text-to-image/workdir/text_to_image/" 
                                     + "data/flowers", base_size=64,
                                   split="train", transform=None,
                                   target_transform=None)
        with tf.Session() as sess:
            saver = tf.train.Saver(max_to_keep=5)
            checkpoint_dir="./checkpoints"
            ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
            if ckpt and ckpt.model_checkpoint_path:
                sess.run(tf.global_variables_initializer())
                ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
                saver.restore(sess, os.path.join(checkpoint_dir, ckpt_name))
                print(" [*] Success to read {}".format(ckpt_name))
            else:
                print("[*] ERROR: Could not load checkpoint!")    
            # data_batch: list of tuples: imgs, caps, cap_len, cls_id, key
            data_batch=dataset.__get_next_batch_ordered__(split="train")
            word_embeddings = list()
            sent_embeddings = list()
            while data_batch:
                imbatch, capsbatch, cap_lens_batch, class_mask = \
                                             self.prepare_data_batch(data_batch)
                feed_dict={
                    self.is_training: False,
                    self.imgs: imbatch,
                    self.caps: capsbatch,
                    self.cap_lens: cap_lens_batch,
                    self.class_mask: class_mask
                }        
    
                word_emb, sent_emb = sess.run(fetches=[self.wordf, self.sentf],
                                                feed_dict=feed_dict)
                data_batch = dataset.__get_next_batch_ordered__(split="train")    
                word_embeddings.append(word_emb) 
                sent_embeddings.append(sent_emb) 
            word_embeddings = unbatch_embeddings(word_embeddings, 
                                             10 * dataset.train_number_example)
            sent_embeddings = unbatch_embeddings(sent_embeddings, 
                                             10 * dataset.train_number_example)
            if not os.path.exists("embeddings"):
                os.makedirs("embeddings")
            with open("embeddings/train_word_embeddings.pkl", "wb") as emb_file:
                pickle.dump(word_embeddings, emb_file)
            with open("embeddings/train_sent_embeddings.pkl", "wb") as emb_file:
                pickle.dump(sent_embeddings, emb_file)

            data_batch=dataset.__get_next_batch_ordered__(split="test")
            word_embeddings = list()
            sent_embeddings = list()
            while data_batch:
                imbatch, capsbatch, cap_lens_batch, class_mask = \
                                             self.prepare_data_batch(data_batch)
                feed_dict={
                    self.is_training: False,
                    self.imgs: imbatch,
                    self.caps: capsbatch,
                    self.cap_lens: cap_lens_batch,
                    self.class_mask: class_mask
                }        
                word_emb, sent_emb = sess.run(fetches=[self.wordf, self.sentf],
                                                feed_dict=feed_dict)
                data_batch = dataset.__get_next_batch_ordered__(split="test")    
                word_embeddings.append(word_emb) 
                sent_embeddings.append(sent_emb) 
            word_embeddings = unbatch_embeddings(word_embeddings, 
                                             10 * dataset.test_number_example)
            sent_embeddings = unbatch_embeddings(sent_embeddings, 
                                             10 * dataset.test_number_example)
            with open("embeddings/test_word_embeddings.pkl", "wb") as emb_file:
                pickle.dump(word_embeddings, emb_file)
            with open("embeddings/test_sent_embeddings.pkl", "wb") as emb_file:
                pickle.dump(sent_embeddings, emb_file)
     
           
    def get_img_features(self, data_batch):
        with tf.Session() as sess:
            saver = tf.train.Saver(max_to_keep=5)
            checkpoint_dir="./checkpoints"
            ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
            if ckpt and ckpt.model_checkpoint_path:
                sess.run(tf.global_variables_initializer())
                ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
                saver.restore(sess, os.path.join(checkpoint_dir, ckpt_name))
                print(" [*] Success to read {}".format(ckpt_name))
            else:
                print("[*] ERROR: Could not load checkpoint!")    
            # data_batch: list of tuples: imgs, caps, cap_len, cls_id, key
            imbatch, capsbatch, cap_lens_batch, class_mask = \
                                             self.prepare_data_batch(data_batch)
            feed_dict={
                self.is_training: False,
                self.imgs: imbatch,
                self.caps: capsbatch,
                self.cap_lens: cap_lens_batch,
                self.class_mask: class_mask
            }        
    
            img_localf, img_globalf = sess.run(fetches=[self.img_localf,
                                                        self.img_globalf],
                                                feed_dict=feed_dict)
            return img_localf, img_globalf


    def get_predictions(self, data_batches):
        with tf.Session() as sess:
            saver = tf.train.Saver(max_to_keep=5)
            checkpoint_dir="./checkpoints"
            ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
            if ckpt and ckpt.model_checkpoint_path:
                sess.run(tf.global_variables_initializer())
                ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
                saver.restore(sess, os.path.join(checkpoint_dir, ckpt_name))
                print(" [*] Success to read {}".format(ckpt_name))
            else:
                print("[*] ERROR: Could not load checkpoint!")    
            # data_batch: list of tuples: imgs, caps, cap_len, cls_id, key
            scores0, scores1, itm_scores = [], [], []
            for data_batch in data_batches:
                imbatch, capsbatch, cap_lens_batch, class_mask = \
                                             self.prepare_data_batch(data_batch)
                feed_dict={
                    self.is_training: False,
                    self.imgs: imbatch,
                    self.caps: capsbatch,
                    self.cap_lens: cap_lens_batch,
                    self.class_mask: class_mask
                }        
    
                s0, s1, itms = sess.run(fetches=[self.scores0,
                                                        self.scores1,
                                                        self.itm_scores],
                                                feed_dict=feed_dict)
                scores0.append(s0)
                scores1.append(s1)
                itm_scores.append(itms)
            return scores0, scores1, itm_scores

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
                               description="Train DAMSM or perform inference")
    parser.add_argument("--training", help="perform training",
                        dest="training", action="store_true")
    parser.add_argument("--inference", help="perform inference",
                        dest="inference", action="store_true")
    parser.add_argument("--get_embeddings", help="extract embeddings using" \
                        "the pretrained model", dest="embeddings",
                        action="store_true")
    args = parser.parse_args()

    d = DAMSM(bs=50)
    dataset = TextDataset.TextDataset(
                            data_dir="/home/users/tilic/project/" 
                                     + "text-to-image/workdir/text_to_image/" 
                                     + "data/flowers", base_size=64,
                                   split="train", transform=None,
                                   target_transform=None)
    
    if args.training:
        print("Argstraining is true")
        loss, opt = d.train(dataset, 50)
    elif args.inference:
        data_batches = []
        for _ in range(dataset.train_number_example // 50):
            data_batches.append(dataset.get_next_batch(50))
        s0, s1, itm = d.get_predictions(data_batches)
        with open("scores0_train.pkl", "wb") as f:
            pickle.dump(s0, f)
        with open("scores1_train.pkl", "wb") as f:
            pickle.dump(s1, f)
        with open("itm_train.pkl", "wb") as f:
            pickle.dump(itm, f)
        data_batches = []
        for _ in range(dataset.test_number_example // 50):
            data_batches.append(dataset.get_next_batch(50, "test"))
        s0, s1, itm = d.get_predictions(data_batches)
        with open("scores0_test.pkl", "wb") as f:
            pickle.dump(s0, f)
        with open("scores1_test.pkl", "wb") as f:
            pickle.dump(s1, f)
        with open("itm_test.pkl", "wb") as f:
            pickle.dump(itm, f)
    
    elif args.embeddings:
        d.extract_embeddings()
