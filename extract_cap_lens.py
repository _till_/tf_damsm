import TextDataset
import pickle
import numpy as np

dataset = TextDataset.TextDataset(data_dir="/home/users/tilic/project/" 
                                     + "text-to-image/workdir/text_to_image/" 
                                     + "data/flowers", base_size=64,
                                   split="train", transform=None,
                                   target_transform=None)

"""
data_batch=dataset.__get_next_batch_ordered__(split="test")
cap_lens = []
while data_batch:
    for start in [0, 10, 20, 30, 40]:
        lens = []
        for x in data_batch[start:start+10]:
            lens.append(x[3])
        cap_lens.append(np.vstack(lens))
    data_batch=dataset.__get_next_batch_ordered__(split="test")
print(cap_lens[0])
with open("cap_lens_test.pkl", "wb") as cap_len_file:
    pickle.dump(cap_lens, cap_len_file)
"""
data_batch=dataset.__get_next_batch_ordered__(split="train")
cap_lens = []
while data_batch:
    for start in [0, 10, 20, 30, 40]:
        lens = []
        for x in data_batch[start:start+10]:
            lens.append(x[3])
        cap_lens.append(np.vstack(lens))
    data_batch=dataset.__get_next_batch_ordered__(split="train")
print(len(cap_lens))
with open("cap_lens_train.pkl", "wb") as cap_len_file:
    pickle.dump(cap_lens, cap_len_file)
