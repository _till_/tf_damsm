import tensorflow as tf
import numpy as np

def masked_softmax(logits, mask):
        """
        Adapted from https://github.com/tensorflow/tensorflow/issues/11756

        Masked softmax over dim 1, mask broadcasts over dim 2
        :param logits: (N, L, T)
        :param mask: (N, L)
        :return: probabilities (N, L, T)
        """
        # 289 (num. img-features)
        v = tf.shape(logits)[2]
        
        # indices that should be filled with inf
        # nr. of Trues x 2
        indices = tf.cast(tf.where(tf.logical_not(mask)), tf.int32)
        
        inf = tf.constant(np.array([[tf.int32.max]], dtype=np.float32),
                          dtype=tf.float32)
        
        # nr. of Trues x 289
        infs = tf.tile(inf, [tf.shape(indices)[0], v])
        
        # bs x 18 x 289
        # put in [inf]*289 at position [batch, word], where word is padding  
        infmask = tf.scatter_nd(
            indices=indices,
            updates=infs,
            shape=tf.shape(logits))
        
        _p = tf.nn.softmax(logits - infmask, axis=1)
        return _p


def unbatch_embeddings(emb_batches, num_embeddings):
    """
    Takes the embedding batches resulting from running the DAMSM model
    and collects them in one list. Removes the embeddings resulting from
    padding the final batch.
    """
    unbatched_embeddings = []
    for batch in emb_batches:
        for emb in np.vsplit(batch, batch.shape[0]):
            unbatched_embeddings.append(np.squeeze(emb))
    print("Returning unbatched embeddings")
    return unbatched_embeddings[:num_embeddings]
